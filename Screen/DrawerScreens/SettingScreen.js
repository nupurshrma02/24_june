import React, { useState } from 'react';
import { View, Text, StyleSheet, TextInput, TouchableOpacity, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import AsyncStorage from '@react-native-community/async-storage';


const SettingScreen = ({ navigation }) => {
  
  const [name, setName] = useState('');
  const [age, setAge] = useState('');
  const [pn, setPn] = useState('');
  const [email, setEmail] = useState('');


  const myfun = async() => {
    let user = await AsyncStorage.getItem('userdetails');
    user = JSON.parse(user);
    //let user_id= setUserid(user.id);
    console.log('user id=>', user.id)
    //Alert.alert(petname);
    //let user_id = {setUserid}
    let dataFetchUrl = 'http://3.12.158.241/medical/api/userDetails/'+user.id;
    console.log('userDetailUrl---->', dataFetchUrl);
    await fetch(dataFetchUrl,{
        method : 'POST',
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          "name": name,
          //"email": email,
          "mobile": pn,
          "age": age,
          //"age"     :Age,
          //"address" :Address,
                      
     })
    }).then(res => res.json())
    .then(resData => {
       console.log(resData);
       //Alert.alert(resData.message);
       if(resData.msg === 'User Details Created'){
        Alert.alert("User Details Added");
        //navigation.navigate('deliveryAddressStack')
       }
    });
 }
  

    return (
      <View style={styles.container}>
      <View style={{marginTop: 10, marginLeft: 10}}>
        <Text style={styles.TextStyle}> Name </Text>
      </View>
      <View style={{}}>
        <TextInput 
        value={name}
        onChangeText={name => setName(name)}
        style={styles.Input}/>
      </View>
      <View style={{marginTop: 10, marginLeft: 10}}>
        <Text style={styles.TextStyle}> Age </Text>
      </View>
      <View style={{}}>
        <TextInput 
        value={age}
        onChangeText={age => setAge(age)}
        style={styles.Input}/>
      </View>
      <View style={{marginTop: 10, marginLeft: 10}}>
        <Text style={styles.TextStyle}> Phone Number </Text>
      </View>
      <View style={{}}>
        <TextInput 
        value={pn}
        onChangeText={pn => setPn(pn)}
        style={styles.Input}/>
      </View>
      {/* <View style={{marginTop: 10, marginLeft: 10}}>
        <Text 
         style={styles.TextStyle}> email </Text>
      </View>
      <View style={{}}>
        <TextInput 
        value={email}
        onChangeText={email => setEmail(email)}
        style={styles.Input}/>
      </View> */}
      {/* <TouchableOpacity onPress={()=> this.props.navigation.navigate('AddressStack')} style={styles.appButtonContainer}>
         <Text style={styles.appButtonText}>Add Address</Text>
      </TouchableOpacity> */}
      <View style={styles.InputText}>
      <Text style={{fontSize: 15}}>Address</Text>
      <TouchableOpacity onPress={()=> navigation.navigate('DeliveryAddress')}>
      <Icon name="chevron-right" size={20} color="#000" style={{marginLeft: 300}}/>
      </TouchableOpacity>
      </View>
      <View style={{alignItems: 'center'}}>
      <TouchableOpacity onPress={myfun} style={styles.appButtonContainer}>
         <Text style={styles.appButtonText}>Save</Text>
      </TouchableOpacity>
      </View>
      </View>
      
    );
  }
  export default SettingScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: `#dcdcdc`,
  },
  TextStyle: {
    fontWeight : '100',
    fontSize : 15,
    
  },
  Input : {
    borderColor : '#a9a9a9',
    borderWidth: 1,  
    height: 40,  
    margin: 10,  
    padding: 10, 
    backgroundColor: `#f8f8ff`,
  },
  appButtonContainer: {
    elevation: 8,
    backgroundColor: "#1e90ff",
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 12,
    marginTop: 60,
    width: '50%',
    marginLeft: 10,
  },
  appButtonText: {
    fontSize: 18,
    color: "#fff",
    fontWeight: "bold",
    alignSelf: "center",
    textTransform: "uppercase"
  },
  InputText:{
    //flex: 1, 
    flexDirection: 'row', 
    alignItems: 'center',
    borderColor : '#a9a9a9',
    borderWidth: 1,  
    height: 40,  
    margin: 10,  
    padding: 10, 
    backgroundColor: `#f8f8ff`,
    marginTop: 35,
  }

})
