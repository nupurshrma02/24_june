//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native';
import { RadioButton } from 'react-native-paper';
import CheckBox from '@react-native-community/checkbox';

// create a component
const orderInfo = () => {
    const [checked, setChecked] = React.useState('');
    const [toggleCheckBox, setToggleCheckBox] = React.useState(false)

    return (
    <ScrollView>
    <View style={styles.container}>
        <Text style={styles.TextStyle}>orderInfo</Text>
    <View>
      <View style={styles.options}>
      <RadioButton
        value="first"
        status={ checked === 'first' ? 'checked' : 'unchecked' }
        onPress={() => setChecked('first')}
        color='#1e90ff'
      />
      <Text style={styles.containerText}>Order everything as per prescription</Text>
      </View>
      <View style={styles.options}>
      <RadioButton
        value="second"
        status={ checked === 'second' ? 'checked' : 'unchecked' }
        onPress={() => setChecked('second')}
        color='#1e90ff'
      />
      <Text style={styles.containerText}>Search and add medicines to cart</Text>
      </View>
      <View style={styles.options}>
      <RadioButton
        value="second"
        status={ checked === 'third' ? 'checked' : 'unchecked' }
        onPress={() => setChecked('third')}
        color='#1e90ff'
      />
      <Text style={styles.containerText}>Call me for details</Text>
      </View>
      </View>
    </View>
    <Text style={{marginTop:10, marginLeft:10,}}>Note: We dispense full strips of tablets/capsules</Text>
    <View>
        <Text style={styles.TextStyle}> Lab Tests </Text>
        <View style={styles.options}>
        <CheckBox
        disabled={false}
        value={toggleCheckBox}
        onValueChange={(newValue) => setToggleCheckBox(newValue)}
        />
        <Text style={styles.containerText}>Also include lab tests</Text>
        </View>
    </View>
    </ScrollView>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        //justifyContent: 'center',
        //alignItems: 'center',
        backgroundColor: '#dcdcdc',
    },
    TextStyle: {
        fontSize: 15,
        color: "#696969",
        fontWeight: "bold",
        textTransform: "uppercase",
        marginTop: 10,
        marginLeft: 10,
    },
    options: {
      backgroundColor: '#fff',
      padding: 15,
      marginTop: 10,
      paddingLeft: 10,
      marginLeft: 10,
      marginRight: 10,
      shadowColor: 'grey',
      borderColor: 'grey',
      borderWidth: 1,
      flexDirection: 'row',
    },
    containerText:{
      fontSize: 17,
      paddingHorizontal: 10,
      paddingVertical: 5,
      fontWeight: 'bold',
    },
});

//make this component available to the app
export default orderInfo;
